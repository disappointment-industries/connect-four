# Webprogramozás - számonkérés

Nag Anna Fruzsina (G76NQF)

Ezt a megoldást a fent írt hallgató küldte be és készítette a Webprogramozás kurzus számonkéréséhez.  
Kijelentem, hogy ez a megoldás a saját munkám. Nem másoltam vagy használtam harmadik féltől 
származó megoldásokat. Nem továbbítottam megoldást hallgatótársaimnak, és nem is tettem közzé.   
Az Eötvös Loránd Tudományegyetem Hallgatói Követelményrendszere 
(ELTE szervezeti és működési szabályzata, II. Kötet, 74/C. §) kimondja, hogy mindaddig, 
amíg egy hallgató egy másik hallgató munkáját - vagy legalábbis annak jelentős részét - 
saját munkájaként mutatja be, az fegyelmi vétségnek számít.   
A fegyelmi vétség legsúlyosabb következménye a hallgató elbocsátása az egyetemről.

---
## Értékelés

### Az alap feladatok (35 pont)

[ ] A "További elvárások" részben szereplő `README.md` fájl megfelelően kitöltve szerepel a feltöltött csomagban (0 pont)  
[ ] A játékoldal megjelenik. (2 pont)  
[ ] Felváltva lehet korongokat dobálni. (5 pont)  
[ ] A nyerést vagy döntetlent érzékeli a gép és kiírja. (5 pont)  
[ ] A játék nyitólapjáról elérhető a játék leírása (2 pont)  
[ ] A játék nyitólapján a játékosok nevének megadása (2 pont) (2 pont)  
[ ] A Start gombra kattintva új játék indítható. (4 pont)  
[ ] A játékoldalon az oszlop kiválasztása történhet egérrel vagy billentyűzettel. (10 pont)  
[ ] A játékoldalon a korong animálva esik le. (5 pont)  
[ ] **Késés naponta (-2 pont)**  

### Plusz feladatok (plusz 10 + 5 pont)

[ ] Statisztika vezetése és megjelenítése (4 pont)  
[ ] Játék mentésének lehetősége (4 pont)  
[ ] 1 játékos mód (+3 pont)  
[ ] Időlimit (+2 pont)  
