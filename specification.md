# Specifikáció

## A játék menete

A játéknak kétféle felhasználói felülete van:

- a játék nyitólapja, ami kezdetben jelenik meg, és a beállításokat tartalmazza; és
- a játékoldal, ahol játszani lehet.

Ugyan ezeket "oldal"-aknak hívjuk, mégsem kell ezeket külön HTML oldalon megvalósítani. Sőt, elvárás, hogy mindez egy HTML oldalon belül kerüljön megvalósításra, és pl. a kétféle "panel" megjelenésének változtatgatásával (pl. egy div hidden tulajdonságának programozásával) menjük egyik játékfázisból a másikba.

### A játék nyitólapja

A játék nyitólapján jelenjen meg

- a játék neve, legyen lehetőség
- a játékszabály elolvasására (ugyanott, külön oldalon, elrejtve/felfedve, stb. módon).

**Két játékos** van. A nyitólapon megadható a nevük, alapértelmezetten "1. játékos" és "2. játékos" legyen.

Egy **Start** feliratú gomb lenyomására indul a játék.

## Játékoldal

- A játék indítása után megjelenik az üres tábla.
- A felületen jelezni kell, hogy melyik játékos van soron. (Ezt talán legegyszerűbb a bedobandó korong színével jelezni.) Mindig az 1. játékos kezd.
- Korong dobásához az alábbi két lehetőség közül az egyiket kell megvalósítani:
  -  Az egeret a tábla felett húzogatva, az adott oszlop felett megjelenik a bedobandó korong. Kattintásra a korong bedobásra kerül.
  -  A korong pozícióját a jobbra-balra nyilakkal mozgathatjuk. Space vagy ENTER megnyomására a korong bedobásra kerül.
- A korong animálva kerüljön a helyére, azaz lássuk leesni.
- Ha valamelyik játékosnak vízszintesen, függőlegesen vagy átlósan összejön négy egyforma színe, akkor az a játékos nyer, és a játék véget ér. A győztes játékos nevét írjuk ki. Ha döntetlen, akkor ezt kell kiírni. Majd egy kattintásra térjünk vissza a nyitólapra.

## Plusz feladatok plusz pontokért

- **Statisztika nyilvántartása**
  -   A főoldalon legyen egy lista a korábbi játékospárokkal feltöltve. A lista tartalmazza az adott pár nevét, illetve hogy hányszor nyert az egyik, hányszor a másik, hányszor volt döntetlen. A sorrend is fontos lehet, azaz Piroska-Farkas és Farkas-Piroska különböző pár, mert mindig az 1. játékos kezd. A lista egy elemére kattintva a nevek automatikusan kitöltődnek. (Pl. Piroska-Farkas párra kattintva az első játékos helyére a Piroska, a 2. helyére a Farkas név kerül.) Ezzel megkönnyítjük a visszatérő játékospárok nevének beírását.
- **Játék mentése és mentett játék folytatása**
  -   Ha vannak félbehagyott játékok, akkor azoknak a listája a főoldalon jelenik meg. A lista egy eleme a mentés dátumát, és a kitöltés %-os arányát tartalmazza. Rákattintva az adott állás töltődik be.
- **1 játékos mód:**
    a játék nyitólapján lehessen ezt kiválasztani.
    ekkor a második játékos a gép lesz, aki valamilyen logika szerint dobja a korongot
    a gép is emberi idő alatt végezze a dolgát, azaz kis idő teljen el az oszlop kiválasztása és a dobás között
- **Időlimit**
    - lehessen megadni a főoldalon egy időlimitet, amennyit egy-egy játékos összesen gondolkodhat a játék során. Ha ez letelt, akkor automatikusan veszít.

## További elvárások és segítség

Fontos az **igényes megjelenés**. Ez nem feltétlenül jelenti egy agyon csicsázott oldal elkészítését, de azt igen, hogy 1024x768 felbontásban és a fölött az elrendezés jól jelenjen meg, a kártyákban középre rendezetten és felismerhetően jelenjenek meg az ábrák. Ehhez lehet minimalista designt is alkalmazni, lehet különböző háttérképekkel és grafikus elemekkel felturbózott saját CSS-t készíteni, de lehet bármilyen CSS keretrendszer segítségét is igénybe venni.

Nincs elvárás arra vonatkozóan, hogy milyen **technológiá**val (táblázat, div-ek vagy canvas) oldod meg a feladatot, továbbá a megjelenést és működést illetően sincsenek kőbe vésett elvárások. A lényeg, hogy a fenti feladatok felismerhetők legyenek, és a játék jól játszható legyen.

Az időméréshez lehet használni a `Date.now()` metódust, ami ms-okban adja vissza az aktuális időt. A folyamatosan időkijelzéshez használj időzítőt!

### A fejlesztés lépésekre bontása

Szeretnénk azoknak is segítséget nyújtani, akiknek nehezebb egy nagyobb feladatot átlátni, megtervezni. Lehet az egész feladatot előre megtervezni, és utána fejleszteni, de az alábbi lépések kisebb részfeladatok megoldásánál is használhatók:

- Készítsd el a fejlesztendő alkalmazás statikus HTML prototípusát! Azaz első lépésben csak HTML és CSS segítségével tervezd meg a nyitóoldal és a játékoldal összes elemét: hogyan adod meg a beállításokat, hogyan jelenik meg a játéktábla, fölötte hogyan jelenik meg a korong, stb. Bizonyos műveletekhez hozz létre egy olyan stílusosztályt, ami a műveletnek megfelel, és statikusan alkalmazd a stílusosztályt az elemre, hogy lásd, tényleg működik-e.
- Gondold át, hogy az egyes részfeladatok milyen adatszerkezettel írhatók le!
  -   Hogyan tárolod a beállításokat?
  -   Hogyan tárolod a táblát?
  -   Hogyan tárolod a mentéseket, vagy a korábbi játékokat?
  -   Honnan tudod, hogy a nyitóoldalon vagy a játékoldalon vagy-e?
  -   stb.
- Gondold át, hogy a felületeddel a felhasználó milyen módon lép interakcióba, mire kattinthat, stb, és ezeket hogyan kezeled. Azaz határozd meg az eseménykezelőket! Ezek lesznek a mini programjaid.

Egy nagyobb feladatnál nem látunk át előre mindent. A fenti lépéseket lehet részfeladatonként alkalmazni. A HTML, CSS fázist nem kell feltétlenül kis lépésekre bontani. Meg lehet előre tervezni az egész felületet. A JavaScript logika fejlesztésénél viszont érdemes kis lépésekben haladni. Egyszerre egy dolog működjön.