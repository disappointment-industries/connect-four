const content = document.getElementById("content");

const x = 7;
const y = 6;

let currentPlayer = "yellow";

let home = document.getElementById("home");
let szabaly = document.getElementById("szabaly");
let game = document.getElementById("game");
let btn_szabaly = document.getElementById("btn_szabaly");
let btn_home = document.getElementById("btn_home");
let btn_submit = document.getElementById("submit");

btn_home.addEventListener('click', loadHomePage);
function loadHomePage() {
    home.hidden = false;
    szabaly.hidden = true;
    game.hidden = true;
}

btn_szabaly.addEventListener('click', loadSzabaly);
function loadSzabaly() {
    home.hidden = true;
    szabaly.hidden = false;
    game.hidden = true;
}

btn_submit.addEventListener('click', loadGame);
function loadGame() {
    home.hidden = true;
    szabaly.hidden = true;
    initGame();
    game.hidden = false;
}
const select = document.createElement("tr");
let rows = [];

function initGame() {
    content.innerHTML = "";
    rows = [];
    dropped.innerHTML = "";
    currentPlayer = "yellow";
    for (let i = 0; i<x; i++) {
        const dot = document.createElement("td");
        dot.classList.add("dot");
        dot.classList.add(currentPlayer);
        dot.classList.add("select");
        dot.onclick = dotCreator(i);
        select.appendChild(dot);
    }
    content.appendChild(select);

    for (let i = 0; i < y; i++) {
        const row = document.createElement("tr");

        for (let j = 0; j < x; j++) {
            const dot = document.createElement("td");
            dot.classList.add("dot");
            dot.dataset.state = "empty";
            row.appendChild(dot);
        }

        rows.push(row);
        content.appendChild(row);
    }

}

const dropped = document.getElementById("dropped");

function dotCreator(X) {
    return function () {
        const divPos = dropped.getBoundingClientRect();
        const startpos = select.childNodes[X].getBoundingClientRect();

        let last = rows.length;
        for (let i = 0; i<rows.length; i++) {
            const row = rows[i];
            const dot = row.childNodes[X];
            if (dot.dataset.state !== "empty") {
                last = i;
                break;
            }
        }
        const endpos = rows[last-1].childNodes[X].getBoundingClientRect();
        
        const elem = document.createElement("span");
        elem.classList.add("dot");
        elem.classList.add(currentPlayer);
        elem.classList.add("dropped");
        elem.style.top = `${startpos.y-divPos.y}px`;
        elem.style.left = `${startpos.x-divPos.x}px`;
        dropped.appendChild(elem);
        setTimeout(() => {
            elem.style.top = `${endpos.y-divPos.y}px`;
        }, 200);

        rows[last-1].childNodes[X].dataset.state = currentPlayer;
        
        const nextPlayer = currentPlayer === "yellow" ? "red" : "yellow";
        select.childNodes.forEach(s => {
            s.classList.remove(currentPlayer);
            s.classList.add(nextPlayer);
        });

        if (winChecker()) {
            const win = currentPlayer;
            setTimeout(() => {
                alert(`${win} wins!`);
                loadHomePage();
            }, 400);

        }

        currentPlayer = nextPlayer;
    }
}

function winChecker() {
    for (let X = 0; X < x; X++) {
        for (let Y = 0; Y < y; Y++) {
            const state = rows[Y].childNodes[X].dataset.state;
            if (state !== "empty") {
                console.log(`${X}, ${Y}: ${state}`);
            }
        }
    }
    
    //sorok
    for (let Y = 0; Y < y; Y++) {
        for (let i = 0; i <= x - 4; i++) {
            let win = true;
            for (let X = i; X < i + 4; X++) {
                if (rows[Y].childNodes[X].dataset.state !== currentPlayer) {
                    win = false;
                    break;
                }
            }
            if (win){
                return true;
            }
        }
    }
    
    //oszlopok
    for (let X = 0; X < x; X++) {
        for (let i = 0; i <= y - 4; i++) {
            let win = true;
            for (let Y = i; Y < i + 4; Y++) {
                if (rows[Y].childNodes[X].dataset.state !== currentPlayer) {
                    win = false;
                    break;
                }
            }
            if (win){
                return true;
            }
        }
    }

    //jobbra-le
    for (let X = 0; X <= x-4; X++) {
        for (let Y = 0; Y <= y-4; Y++) {
            let _x = X;
            let _y = Y;
            let win = true;
            for (let i = 0; i < 4; i++) {
                if (rows[_y].childNodes[_x].dataset.state !== currentPlayer) {
                    win = false;
                    break;
                }
                _x++;
                _y++;
            }
            if (win){
                return true;
            }
        }
    }

    //balra-le
    for (let X = 4; X < x; X++) {
        for (let Y = 0; Y <= y-4; Y++) {
            let _x = X;
            let _y = Y;
            let win = true;
            for (let i = 0; i < 4; i++) {
                if (rows[_y].childNodes[_x].dataset.state !== currentPlayer) {
                    win = false;
                    break;
                }
                _x--;
                _y++;
            }
            if (win){
                return true;
            }
        }
    }
}
